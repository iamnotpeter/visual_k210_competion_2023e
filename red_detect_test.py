"""
@file:    red_detect_2023e.py
@author:  榴莲派工作室(LLP STUDIO)SVP Peng
@version: V1.0
@date:    2023-08-02
@brief:   识别黑色胶带上的红色激光点
"""

import sensor
import image
import lcd
import time
import gc

lcd.init()
sensor.reset()
sensor.set_pixformat(sensor.RGB565)
sensor.set_framesize(sensor.QVGA)    # 320 * 240
sensor.skip_frames(time=900)
sensor.set_auto_exposure(False, 30000)    # 在这里调节曝光度，调节完可以比较清晰地看清激光点
sensor.set_auto_whitebal(False)
sensor.set_auto_gain(False)    # 关闭增益（色块识别时必须要关）

RED_THRESHOLD = [(55, 76, 13, 15, -1, 3)]
#RED_THRESHOLD = [(56, 100, 45, 127, -128, 127)]


def find_max(blobs):
    """寻找目标色块中最大的色块

    Args:
        blobs: 目标色块群

    Returns:
        一个blob类对象，返回目标色块群中的最大色块
    """
    max_size = 0
    for blob in blobs:
        if blob.pixels() > max_size:
            max_blob = blob
            max_size = blob.pixels()
    return max_blob


while True:
    cx = 0
    cy = 0
    img = sensor.snapshot()
    blobs = img.find_blobs(RED_THRESHOLD, x_stride=1, y_stride=1, area_threshold=0,
        pixels_threshold=0, merge=False, margin=1)
    if blobs:
        blob = find_max(blobs)
        img.draw_rectangle(blob[0:4])
        cx = blob[5]
        cy = blob[6]
        #print(cx, cy)
        img.draw_cross(cx, cy, color=255)
        img.draw_string(cx, cy, "{},{}".format(cx, cy), color=255)
    # 12位数据帧，帧头2位，红色中心进位2位，红色中心(rcx, rcy)，绿色中心进位2位，绿色中心(gcx, gcy)，帧尾2位
    #data = bytearray([0xa3, 0xb3, int(rcx > 255), int(rcy > 255), rcx, rcy,
        #int(gcx > 255), int(gcy > 255), gcx, gcy, 0x0d, 0x0a])
    #print(data)
    lcd.display(img)
gc.collect()
